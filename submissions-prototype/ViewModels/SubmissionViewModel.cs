﻿using System;
using System.Collections.Generic;
using submissionsprototype.Models;

namespace submissionsprototype.ViewModels
{
    public class SubmissionViewModel
    {
        public SubmissionViewModel()
        {
        }

        public IEnumerable<Submission> Submissions { get; set; }


    }
}
