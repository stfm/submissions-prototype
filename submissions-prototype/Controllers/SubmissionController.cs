﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using submissionsprototype.Models;
using submissionsprototype.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace submissionsprototype.Controllers
{
    public class SubmissionController : Controller
    {

        private readonly ISubmissionRepository _submissionRepository;

        public SubmissionController(ISubmissionRepository submissionRepository) {

            _submissionRepository = submissionRepository;
        }

        // GET: /<controller>/
        public IActionResult Index()
        {

            var submissionViewModel = new SubmissionViewModel
            {

                Submissions = _submissionRepository.Submissions
                                                   
            };

            return View(submissionViewModel);

        }

        public IActionResult Create() {

            return View();

        }

        [HttpPost]
        public IActionResult Create(Submission submission) {

            _submissionRepository.CreateSubmission(submission);

            return RedirectToAction("CreateComplete");

        }

        public IActionResult CreateComplete() {


            return View();

        }
    }
}
