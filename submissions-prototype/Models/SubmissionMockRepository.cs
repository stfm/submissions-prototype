﻿using System;
using System.Collections.Generic;

namespace submissionsprototype.Models
{
    public class SubmissionMockRepository : ISubmissionRepository
    {
        public SubmissionMockRepository()
        {
        }

        public IEnumerable<Submission> Submissions
        {

            get
            {

                return new List<Submission> {

                    new Submission { SubmissionId=1, SubmissionTitle="The First Submission"},
                    new Submission { SubmissionId=2, SubmissionTitle="The Second Submission"}



                };


            }

        }

        public void CreateSubmission(Submission submission) {

            //do nothing in mock
        }

    }
}
