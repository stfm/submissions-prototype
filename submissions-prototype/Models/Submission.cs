﻿using System;
namespace submissionsprototype.Models
{
    public class Submission
    {
        public Submission()
        {
        }

        public int SubmissionId { get; set; }
        public string SubmissionTitle { get; set; }

    }
}
