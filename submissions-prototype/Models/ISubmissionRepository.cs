﻿using System;
using System.Collections.Generic;

namespace submissionsprototype.Models
{
    public interface ISubmissionRepository
    {
        IEnumerable<Submission> Submissions { get;  }

        void CreateSubmission(Submission submission);

    }
}
