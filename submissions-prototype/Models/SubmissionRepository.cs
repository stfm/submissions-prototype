﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace submissionsprototype.Models
{
    public class SubmissionRepository : ISubmissionRepository
    {
        private readonly AppDbContext _appDbContext;

        public SubmissionRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Submission> Submissions {

            get
            {

                return _appDbContext.Submissions;

            }

        }

        public void CreateSubmission (Submission submission) {

            _appDbContext.Submissions.Add(submission);

            _appDbContext.SaveChanges();

        }


    }
}
