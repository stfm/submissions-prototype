Submissions Prototype

This is a simple .NET Core 2.0 MVC web application that demonstrates
using Entity Framework Core and data migrations to create and maintain
the database for conference submissions.

This web application uses SQL Server on the officer server for 
its database.

Before building and running the application the first time you 
must do the following to create the database and tables.

1. Open a command window

2.  cd into the project's folder (the folder where the appsettings.json file is located)

3.  enter this command "dotnet ef database update"

You should see output that the conferencetest database and submissions table were created.

You can then run the web application locally.  Visit create submission to create a 
submission.  Then visit view submissions to view the submissions.
